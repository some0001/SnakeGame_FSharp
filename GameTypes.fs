﻿namespace ViewModels

open System.ComponentModel
open Microsoft.FSharp.Quotations.Patterns

type Direction =
    | UP
    | RIGHT
    | DOWN
    | LEFT
    | NONE

type SnakeGameState = 
    | NORMAL
    | CRASH
    | GAMEOVER


module SnakeOperations  =
    type SnakeMoveResult<'TNoCollision,'TSnakeCollision,'TFoodCollision> =
        | NoCollision of 'TNoCollision
        | SnakeCollision of 'TSnakeCollision
        | FoodCollision of 'TFoodCollision

    let (>=>) switch1 switch2 x =
        match switch1 x with
        | NoCollision s -> switch2 s
        | FoodCollision s -> switch2 s
        | SnakeCollision sc -> SnakeCollision sc

type SnakeGame() =
    let mutable lives : Option<int> = Some(3)
    let mutable score : uint32 = 0ul
    let mutable state : SnakeGameState = NORMAL

    member this.Lives with get() = lives
    member this.Score with get() = score
    member this.State with get() = state  and set(newState) = state <- newState

    member this.IncreaseScore(incr) =
        score <- score + incr

    member this.ResetScore() =
        score <- 0ul

    member this.DecreaseLives() =
        if(lives.IsSome) then
            if(lives.Value <= 1) then 
                lives <- None
            else
                lives <- Some(lives.Value - 1)

    member this.ResetLives() =
        lives <- Some(3)

    member this.HasLivesLeft() =
        if lives.IsSome then true else false

    member this.Reset() =
        lives <- Some(3)
        score <- 0ul
        state <- NORMAL

type ObservableObject () =
    let mutable _x = 0;
    let mutable _y = 0;

    let propertyChanged = 
        Event<PropertyChangedEventHandler,PropertyChangedEventArgs>()

    let getPropertyName = function 
        | PropertyGet(_,pi,_) -> pi.Name
        | _ -> invalidOp "Expecting property getter expression"

    interface INotifyPropertyChanged with
        [<CLIEvent>]
        member this.PropertyChanged = propertyChanged.Publish

    member this.NotifyPropertyChanged propertyName = 
        propertyChanged.Trigger(this,PropertyChangedEventArgs(propertyName))

    member this.NotifyPropertyChanged quotation = 
        quotation |> getPropertyName |> this.NotifyPropertyChanged

    member this.X = _x 
    member this.Y = _y

    member this.SetX(v) = 
        _x <- v
        this.NotifyPropertyChanged "X"
    member this.SetY(v) = 
        _y <- v
        this.NotifyPropertyChanged "Y"

type SnakePartObj() = 
    inherit ObservableObject()

type SnakeHead() = 
    inherit SnakePartObj()

type FoodObj() = 
    inherit ObservableObject()

type SnakeHeadUp() = 
    inherit SnakeHead()

type SnakeHeadRight() = 
    inherit SnakeHead()

type SnakeHeadDown() = 
    inherit SnakeHead()

type SnakeHeadLeft() = 
    inherit SnakeHead()

type SnakeBody() = 
    inherit SnakePartObj()

type Food() = 
    inherit FoodObj()

module Helper = 

    let isCounterDirection(d1, d2) =
        let mutable result = false
        match d1 with
        | UP     ->  if d2 = DOWN then result <- true
        | RIGHT  ->  if d2 = LEFT then result <- true
        | DOWN   ->  if d2 = UP then result <- true
        | LEFT   ->  if d2 = RIGHT then result <- true
        result

    let createHead(dir) = 
        match dir with
        | UP -> SnakeHeadUp() :> ObservableObject
        | RIGHT -> SnakeHeadRight() :> ObservableObject
        | DOWN -> SnakeHeadDown() :> ObservableObject
        | LEFT -> SnakeHeadLeft() :> ObservableObject
        | _ -> SnakeHead() :> ObservableObject
